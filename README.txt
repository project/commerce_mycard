Overview
The Commerce MyCard Module provide a payment method for Drupal Commerce Module. MyCard is a 3rd Party Payment Service which can be used at Taiwan, China, Hong kong. The detail of the payment can be found at MyCard Official Website.

Features
This module provide only one payment but with multiple payment method at the mycard side. There are three types of payment. 1. Ingame 2. Costpoint 3. billing.

Requirements
Drupal Commerce
Variables
概覽
這是MyCard的金流模組，主要是整合MyCard的第三方支付到Drupal Commerce模組之中，安裝後並不會直接可以使用，而需要與MyCard的客服聯絡開通後，才會提供對應的Key跟ID。

特色功能
這個模組提供一種整合付款方式，付款時，將客人導到MyCard的介面進行付款，付款的項目會跟與MyCard申請的項目有關。例如：Ingame 點數卡、Costpoint 會員扣點、或其他種類的付款方式。

必須安裝
Drupal Commerce
Variable
注意事項
由於MyCard需要補儲、差異報表等等路徑的設置。這個部分已經在這個模組中處理完畢。唯獨路徑需要照這個模組的規範走。

交易回傳網址：http://yourdomain/checkout/mycard/mcresult
補儲回傳網址：http://yourdomain/checkout/mycard/mcresponse
差異報表回傳網址：http://yourdomain/checkout/mycard/mcdiff
手動補儲網址
由於在特殊交易失敗並且MyCard自動補儲失敗的狀況下，會需要手動補儲值，可以從以下的設定介面進行設定。

路徑：admin/commerce/config/mycard

後面資訊再補齊中
