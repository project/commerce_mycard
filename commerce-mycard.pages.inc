<?php

//這個Function主要是接受MyCard回傳回來的金流訊息
function mycard_confirm_page(){

  global $user;
  global $base_url;
  try {

    //Include all the mycard parameters
    invoke_mycard_module();

    $username=$user->name;
    $userid=$user->uid;
    $key=variable_get('mycard_key');


    //取得POST參數
    $ReturnCode=empty($_POST['ReturnCode'])?'':$_POST['ReturnCode'];
    $ReturnMsg=empty($_POST['ReturnMsg'])?'':$_POST['ReturnMsg'];
    $PayResult=empty($_POST['PayResult'])?'':$_POST['PayResult'];
    $FacTradeSeq=empty($_POST['FacTradeSeq'])?'':$_POST['FacTradeSeq'];
    $PaymentType=empty($_POST['PaymentType'])?'':$_POST['PaymentType'];
    $Amount=empty($_POST['Amount'])?'':$_POST['Amount'];
    $Currency=empty($_POST['Currency'])?'':$_POST['Currency'];
    $MyCardTradeNo=empty($_POST['MyCardTradeNo'])?'':$_POST['MyCardTradeNo'];
    $MyCardType=empty($_POST['MyCardType'])?'':$_POST['MyCardType'];
    $PromoCode=empty($_POST['PromoCode'])?'':$_POST['PromoCode'];
    $Hash=empty($_POST['Hash'])?'':$_POST['Hash'];

    //將訊息decode
    $ReturnMsg=urldecode($ReturnMsg);

    $preHashValue=$ReturnCode.$PayResult.$FacTradeSeq.$PaymentType.$Amount.$Currency.$MyCardTradeNo.$MyCardType.$PromoCode.$key;

    $hash=hash('sha256',$preHashValue);

    if(empty($Hash)){
      throw new Exception(sprintf(MYCARD_WITHOUT_POST));
    }
    elseif($hash!=$Hash){

      throw new Exception(sprintf(MYCARD_HASH_COMPARE_WRONG,$ReturnCode,$ReturnMsg));
    }

    //回傳訂單編號
    $cart_order_id = substr($FacTradeSeq, 14);

    // 更新payment transaction還有訂單狀態
    $condition = array(
      'order_id' => $cart_order_id,
      'remote_id' => $FacTradeSeq,
    );
    $payment_array = commerce_payment_transaction_load_multiple(array(), $condition, false);
    foreach ($payment_array as $key => $value) {
        $transaction = $value;
        $t_status=$value->status;
    }

    if($ReturnCode<>1){ //交易失敗
      throw new Exception(sprintf(MYCARD_TRANSITION_RETURN_MESSAGE,$ReturnCode,$ReturnMsg));
    }
    elseif($PayResult<>3){ //交易失敗，紀錄交易失敗原因
      $transaction->message .='紀錄時間：'.Date('Y-m-d H:i:s',REQUEST_TIME).'</br>
                 ReturnMsg:'.$ReturnMsg.'</br>
                 ReturnCode:'.$MyCardTradeNo.'</br>';
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
      commerce_payment_transaction_save($transaction);
      throw new Exception(sprintf(MYCARD_TRANSITION_RETURN_MESSAGE,$ReturnCode,$ReturnMsg));
    }

    if($PayResult==3){ //回傳交易成功

      //這裡要開始判斷是否真的有這筆紀錄，跟Mycard核對一下資料
      $checkstatus=checkpaymentstatus($FacTradeSeq);
      $requirepayment=require_mycard_payment($FacTradeSeq);
      if($checkstatus<>1){
        throw new Exception(sprintf(MYCARD_TRANSITION_RECHECK_ERROR_MESSAGE,$cart_order_id,$FacTradeSeq,$ReturnCode,$ReturnMsg));
      }
      if($requirepayment<>1){
        throw new Exception(sprintf(MYCARD_TRANSITION_REQUIRE_ERROR_MESSAGE,$cart_order_id,$FacTradeSeq,$ReturnCode,$ReturnMsg));
      }
      if($t_status=='pending'){ //更新交易紀錄為成功，並且修改訂單狀態
        //紀錄成功
        $transaction->message .='紀錄時間：'.Date('Y-m-d H:i:s',REQUEST_TIME).'</br>
                   ReturnMsg:'.$ReturnMsg.'</br>
                   ReturnCode:'.$ReturnCode.'</br>
                   MyCard交易序號:'.$MyCardTradeNo.'</br>
                   付款方式：'.MyCardPaymentType($PaymentType).'</br>';
        $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
        commerce_payment_transaction_save($transaction);

        //完成訂單
        // Get the cart order amount
        $order = commerce_order_load($cart_order_id);
        $order->status='pending';
        commerce_order_save($order);
        commerce_checkout_complete($order);
        drupal_goto('checkout/'.$cart_order_id);
      }
      else{ //已經成功了，直接導入到訂單頁面
          drupal_goto('checkout/'.$cart_order_id);
      }


    }

  }catch(Exception $e) {
    $error = $e->getMessage();
    watchdog('commerce_mycard',$error, array(),WATCHDOG_EMERGENCY);
    echo $error.'</br>';
    //回傳訂單編號
    if(!empty($FacTradeSeq)){
      $cart_order_id = substr($FacTradeSeq, 14);
      $order=commerce_order_load($cart_order_id);
      commerce_order_status_update($order, 'checkout_checkout',FALSE, $revision = NULL, $log = '金流失敗，重設訂單狀態');
      //原本購物頁面
      $link=l('返回結帳頁面','checkout/'.$cart_order_id);
      echo $link;
    }
    exit;
  }

}

//補儲值跑的函數
function mycard_returnrul_page(){
  $DATA=empty($_POST['DATA'])?'':$_POST['DATA'];

  try{

    //Include all the mycard parameters
    invoke_mycard_module();

    if(empty($DATA)){
      throw new Exception(sprintf(MYCARD_RECHARGE_ERROR));
    }
    $DATA=drupal_json_decode($DATA);
    $ReturnCode=$DATA['ReturnCode'];
    $FacServiceId=$DATA['FacServiceId'];
    $TotalNum=$DATA['TotalNum'];
    $FacTradeSeq=$DATA['FacTradeSeq']; //這裡是陣列，一連串的訂單交易序號

    $mycard_facid=variable_get('mycard_facid');

    if($ReturnCode<>1 || $FacServiceId <> $mycard_facid){
      throw new Exception(sprintf(MYCARD_RECHARGE_PARAMETER_ERROR,$ReturnCode,$FacServiceId));
    }

    //更新訂單狀態與交易狀態

    foreach($FacTradeSeq as $key => $value){
      //回傳訂單編號
      $cart_order_id = substr($value, 14);

      $remote_id=$value;

      $condition = array(
        'order_id' => $cart_order_id,
        'remote_id' => $remote_id,
      );
      $payment_array = commerce_payment_transaction_load_multiple(array(), $condition, false);
      foreach ($payment_array as $key2 => $value2) {
          $transaction = $value2;
          $t_status=$value2->status;
      }
      //這裡要開始判斷是否真的有這筆紀錄，跟Mycard核對一下資料
      $checkstatus=checkpaymentstatus($remote_id);
      $requirepayment=require_mycard_payment($remote_id);

      if($checkstatus==1 && $t_status=='pending' && $requirepayment==1){
        //紀錄成功
        $transaction->message .='紀錄時間：'.Date('Y-m-d H:i:s',REQUEST_TIME).'</br>
                   ReturnMsg:'.$ReturnMsg.'</br>
                   ReturnCode:'.$ReturnCode.'</br>
                   MyCard交易序號:'.$ReturnCode.'</br>
                   付款方式：'.MyCardPaymentType($PaymentType).'</br>';
        $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
        commerce_payment_transaction_save($transaction);

        //完成訂單
        // Get the cart order amount
        $order = commerce_order_load($cart_order_id);
        $order->status='pending';
        commerce_order_save($order);
        commerce_checkout_complete($order);
      }
    }
  }
  catch(Exception $e) {
    $error = $e->getMessage();
    watchdog('commerce_mycard',$error, array(),WATCHDOG_EMERGENCY);
    echo $error;
    exit;
  }
}

//差異報表函數
function mycard_diff_page(){
  try{
    if(isset($_POST['StartDateTime']) and isset($_POST['EndDateTime'])){   //代表多筆查詢
      $StartDate=strtotime($_POST['StartDateTime']);
      $EndDate=strtotime($_POST['EndDateTime'].'+1 day');
      $query=db_select('mycard_transition_record','mt');
      $query->condition("mt.TradeDateTime",$StartDate,'>=');
      $query->condition("mt.TradeDateTime",$EndDate,'<');
      $query->fields("mt", array("PaymentType","TradeSeq","MyCardTradeNo","FacTradeSeq","CustomerId","TradeDateTime"));
      $result=$query->execute()->fetchAll();

    }elseif(isset($_POST['MyCardTradeNo'])){ //單筆查詢

      $query=db_select('mycard_transition_record','mt');
      $query->condition("mt.MyCardTradeNo",$_POST['MyCardTradeNo']);
      $query->fields("mt", array("PaymentType","TradeSeq","MyCardTradeNo","FacTradeSeq","CustomerId","TradeDateTime"));
      $result=$query->execute()->fetchAll();

    }else{
      throw new Exception(MYCARD_DIFF_PAGE_ERROR);
    }

    foreach($result as $index => $value){
      $PaymentType=$value->PaymentType;
      $TradeSeq=$value->TradeSeq;
      $MyCardTradeNo=$value->MyCardTradeNo;
      $FacTradeSeq=$value->FacTradeSeq;
      $CustomerId=$value->CustomerId;
      $TradeDateTime=date('Y-m-d',$value->TradeDateTime).'T'.date('H:i:s',$value->TradeDateTime);

      //回傳訂單編號
      $cart_order_id = substr($FacTradeSeq, 14);
      $order=commerce_order_load($cart_order_id);
      //交易金額
      $amount=$order->commerce_order_total['und'][0]['amount'];
      $TotalAmount=commerce_currency_amount_to_decimal($amount,'TWD');
      $Currency='TWD';
      print $PaymentType.','.$TradeSeq.','.$MyCardTradeNo.','.$FacTradeSeq.','.$CustomerId.','.$amount.','.$Currency.','.$TradeDateTime.'<BR>';
    }

  }
  catch(Exception $e){
    watchdog('commerce_mycard',$error, array(),WATCHDOG_EMERGENCY);
    echo $error;
  }
}

?>
