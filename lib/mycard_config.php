<?php
define('MYCARD_TEST_AUTH_URL', 'https://test.b2b.mycard520.com.tw/MyBillingPay/api/AuthGlobal');
define('MYCARD_PRO_AUTH_URL', 'https://b2b.mycard520.com.tw/MyBillingPay/api/AuthGlobal');
define('MYCARD_TEST_PAY_URL', 'https://test.mycard520.com.tw/MyCardPay/');
define('MYCARD_PRO_PAY_URL', 'https://www.mycard520.com.tw/MyCardPay/');
define('MYCARD_TEST_TRADEQUERY_URL','https://test.b2b.mycard520.com.tw/MyBillingPay/api/TradeQuery');
define('MYCARD_PRO_TRADEQUERY_URL','https://b2b.mycard520.com.tw/MyBillingPay/api/TradeQuery');
define('MYCARD_TEST_REQUIRE_PAYMENT_URL','https://test.b2b.mycard520.com.tw/MyBillingPay/api/PaymentConfirm');
define('MYCARD_PRO_REQUIRE_PAYMENT_URL','https://b2b.mycard520.com.tw/MyBillingPay/api/PaymentConfirm');
define('MYCARD_WITHOUT_POST','資料有誤');
define('MYCARD_HASH_COMPARE_WRONG','Hash比對錯誤或MyCard回傳訊息：</br>ReturnCode:%s</br>ReturnMsg: %s');
define('MYCARD_AUTH_RETURN_ERROR', 'MyCard認證錯誤:</br>ReturnCode:%s</br>$ReturnMsg: %s</br>mycard_message: %s)');
define('MYCARD_TRANSITION_RETURN_MESSAGE','MYCARD交易結果回傳訊息</br>ReturnCode:%s</br>$ReturnMsg: %s');
define('MYCARD_TRANSITION_RECHECK_ERROR_MESSAGE','反查MYCARD交易失敗</br>訂單編號:%s</br>廠商交易序號:%s</br>ReturnCode:%s</br>ReturnMsg: %s');
define('MYCARD_TRANSITION_REQUIRE_ERROR_MESSAGE','向MYCARD請款交易失敗</br>訂單編號:%s</br>廠商交易序號:%s</br>ReturnCode:%s</br>ReturnMsg: %s');
define('MYCARD_RECHARGE_ERROR','REQUEST MYCARD的補儲值頁面，但是沒有任何的參數');
define('MYCARD_RECHARGE_PARAMETER_ERROR','REQUEST MYCARD的補儲值頁面，參數有誤</br>ReturnCode:%s</br>FacServiceId:%s');
define('MYCARD_DIFF_PAGE_ERROR','差異報表MyCard丟過來有問題');


function mycard_message($code){
  $codemessage=array(
    '1'=>'要求成功',
    'MBP001'=>'參數有誤',
    'MBP002'=>'查詢資料失敗',
    'MBP003'=>'Hash 值不正確',
    'MBP004'=>'授權失敗',
    'MBP099'=>'授權失敗：通常是發生例外錯誤',
    'MBP006'=>'請款失敗',
    'MBP098'=>'授權碼有誤',
  );
  return $codemessage[$code];
}

function MyCardPaymentType($PaymentType){

  switch ($PaymentType) {
    case 'INGAME':
      return '卡片儲值';
      break;
    case 'COSTPOINT':
      return '會員扣點';
      break;
    default:
      return '其他代碼為小額付費之付費方式';
      break;
  }
}

/**
*   @param $FacTradeSeq:在這裡是屬於Transaciotn的remote_id，也是廠商交易序號
*   @return 1： 代表真的交易成功 0:交易失敗
*   @Do 在這裡會在檢測完畢為成功時，一樣將資料寫到DB裡面
**/
function checkpaymentstatus($FacTradeSeq){

  $AuthCode = db_select("mycard_transition_record", "mt")
        ->fields("mt", array("AuthCode"))
        ->condition("FacTradeSeq", $FacTradeSeq)
        ->execute()
        ->fetchField();

  //預設環境，若沒有設定則用測試環境。
  $status=variable_get('mycard_status','dev');
  if ($status == 'dev') {
    $url=MYCARD_TEST_TRADEQUERY_URL;
  } else {
    $url=MYCARD_PRO_TRADEQUERY_URL;
  }

  $data=array(
    'AuthCode' => $AuthCode,
  );

  //將參數傳到MyCard進行驗證
  $full_url = url($url, array('query' => $data));
  $ch = curl_init();
  $options=array(CURLOPT_RETURNTRANSFER => true,
    CURLOPT_HTTPHEADER => array('Content-type: application/json'),
    CURLOPT_URL => $full_url);
  curl_setopt_array($ch, $options);
  $curl_results = curl_exec($ch); //Getting json result string
  curl_close($ch);
  $result=drupal_json_decode($curl_results);

  $ReturnCode=$result['ReturnCode'];
  $PayResult=$result['PayResult'];
  if($ReturnCode==1 && $PayResult==3){

    //更新mycard_transition_record資料庫資料
    $PaymentType=$result['PaymentType'];
    $MyCardTradeNo=$result['MyCardTradeNo'];
    $MyCardType=$result['MyCardType'];
    $PromoCode=$result['PromoCode'];

    //更新資料庫的資料
    $mycard_transition_update = db_update('mycard_transition_record') // Table name no longer needs {}
     ->fields(array(
       'PaymentType' => $PaymentType,
       'MyCardTradeNo' => $MyCardTradeNo,
       'MyCardType'  => $MyCardType,
       'TradeDateTime' => REQUEST_TIME,
       'PromoCode'   => $PromoCode,
     ))
     ->condition('FacTradeSeq', $FacTradeSeq)
     ->execute();

    return 1;
  }
  else{
    return 0;
  }
}


/**
*   @param $FacTradeSeq:在這裡是屬於Transaciotn的remote_id，也是廠商交易序號
*   @return 1： 代表真的交易成功 0:交易失敗
*   這裡主要是跟MyCard進行請款
**/
function require_mycard_payment($FacTradeSeq){

  $AuthCode = db_select("mycard_transition_record", "mt")
        ->fields("mt", array("AuthCode"))
        ->condition("FacTradeSeq", $FacTradeSeq)
        ->execute()
        ->fetchField();


  //預設環境，若沒有設定則用測試環境。
  $status=variable_get('mycard_status','dev');
  if ($status == 'dev') {
    $url=MYCARD_TEST_REQUIRE_PAYMENT_URL;
  } else {
    $url=MYCARD_PRO_REQUIRE_PAYMENT_URL;
  }

  $data=array(
    'AuthCode' => $AuthCode,
  );

  //將參數傳到MyCard進行驗證
  $full_url = url($url, array('query' => $data));
  $ch = curl_init();
  $options=array(CURLOPT_RETURNTRANSFER => true,
    CURLOPT_HTTPHEADER => array('Content-type: application/json'),
    CURLOPT_URL => $full_url);
  curl_setopt_array($ch, $options);
  $curl_results = curl_exec($ch); //Getting json result string
  curl_close($ch);
  $result=drupal_json_decode($curl_results);
  $ReturnCode=$result['ReturnCode'];
  if($ReturnCode==1){
    return 1;
  }
  else{
    return 0;
  }
}

?>
