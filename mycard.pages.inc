
<?php

function commerce_mycard_recharge_form($form,&$form_state){
  global $user;
  global $base_url;

  //Include all the mycard parameters
  invoke_mycard_module();

  $form['trade_seq']=array(
    '#type'=>'textfield',
    '#title'=>t('廠商交易序號'),
    '#description'=>t('請輸入您要進行手動補儲值的廠商交易序號'),
    '#maxlength' => 255,
    '#attributes' => array('autocomplete' =>'off'),
    '#required' => TRUE,
    '#weight'=>0,
  );

  $form['mycard_recharge_submit']=array(
    '#type'=>'submit',
    '#weight'=>10,
    '#value'=>t('送出補儲資訊'),
  );

  return $form;

}

function commerce_mycard_recharge_form_validate($form,&$form_state){

  $FacTradeSeq=$form_state['values']['trade_seq'];

  //檢查是否有這個序號
  //回傳訂單編號
  $cart_order_id = substr($FacTradeSeq, 14);

  $condition = array(
    'order_id' => $cart_order_id,
    'remote_id' => $FacTradeSeq,
  );
  $payment_array = commerce_payment_transaction_load_multiple(array(), $condition, false);
  if(empty($payment_array)){
    form_set_error('trade_seq',t('廠商序號有誤'));
  }

  $AuthCode = db_select("mycard_transition_record", "mt")
        ->fields("mt", array("AuthCode"))
        ->condition("FacTradeSeq", $FacTradeSeq)
        ->execute()
        ->fetchField();
  if(empty($AuthCode)){
    form_set_error('trade_seq',t('非MyCard交易'));
  }

}

function commerce_mycard_recharge_form_submit($form,&$form_state){

  global $user;
  $username=$user->name;
  //進行判斷並且手動補儲值
  $FacTradeSeq=$form_state['values']['trade_seq'];

  //檢查是否有這個序號
  //回傳訂單編號
  $cart_order_id = substr($FacTradeSeq, 14);

  $condition = array(
    'order_id' => $cart_order_id,
    'remote_id' => $FacTradeSeq,
  );
  $payment_array = commerce_payment_transaction_load_multiple(array(), $condition, false);
  foreach ($payment_array as $key => $value) {
      $transaction = $value;
      $t_status=$value->status;
  }

  //這裡要開始判斷是否真的有這筆紀錄，跟Mycard核對一下資料
  $checkstatus=checkpaymentstatus($FacTradeSeq);
  $requirepayment=require_mycard_payment($FacTradeSeq);

  if($checkstatus==1 && $t_status=='pending' && $requirepayment==1){
    //紀錄成功
    $transaction->message .='紀錄時間：'.Date('Y-m-d H:i:s',REQUEST_TIME).'</br>
               由'.$username.'進行手動補儲值</br>';

    $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
    commerce_payment_transaction_save($transaction);

    //完成訂單
    // Get the cart order amount
    $order = commerce_order_load($cart_order_id);
    $order->status='pending';
    commerce_order_save($order);
    commerce_checkout_complete($order);

    drupal_set_message("補儲值成功");
  }
  else{
    drupal_set_message("尚未付款成功／請款失敗／此筆交易已經成功不用再更新",'warning');
  }
}

//Commerce MyCard的環境參數設定
function commerce_mycard_settings_form($form,&$form_state){

  $form['mycard_status']=array(
    '#type'=>'radios',
    '#title'=>t('MyCard系統狀態'),
    '#description'=>t('請選擇MyCard系統狀態。例如：測試環境DEV,正式環境Production'),
    '#options' => array(
      'dev' => t('測試環境'),
      'production' => t('正式環境'),
    ),
    '#default_value' => !empty(variable_get("mycard_status"))?variable_get("mycard_status"):'dev',
    '#required' => TRUE,
    '#weight'=>0,
  );

  $form['mycard_key']=array(
    '#type'=>'textfield',
    '#title'=>t('MyCard提供的Key'),
    '#description'=>t('MyCard提供的Key，這個Key要跟MyCard的溝通後才會有。'),
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => !empty(variable_get("mycard_key"))?variable_get("mycard_key"):' ',
    '#weight'=>0,
  );

  $form['mycard_facid']=array(
    '#type'=>'textfield',
    '#title'=>t('FacserviceID'),
    '#description'=>t('由MyCard提供的FacserviceID。'),
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => !empty(variable_get("mycard_facid"))?variable_get("mycard_facid"):' ',
    '#weight'=>0,
  );

  $form['submit']=array(
    '#type'=>'submit',
    '#value' => t('儲存')
  );
  return $form;
}

function commerce_mycard_settings_form_submit($form,&$form_state){
  $mycard_status=$form_state['values']['mycard_status'];
  $mycard_key=$form_state['values']['mycard_key'];
  $mycard_facid=$form_state['values']['mycard_facid'];

  variable_set('mycard_status',$mycard_status);
  variable_set('mycard_key',$mycard_key);
  variable_set('mycard_facid',$mycard_facid);

  drupal_set_message(t('儲存成功'));
}

?>
